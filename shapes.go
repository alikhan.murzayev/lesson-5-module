package shapes

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"strconv"
)

type Shape interface {
	Area() float64
}

// General shape with optional fields START

type Shapes struct {
	Shapes []GeneralShape `json:"shapes"`
}

/*
GeneralShape:
A struct that represents all possible shapes and has optional fields
*/
type GeneralShape struct {
	Type  string      `json:"type"`
	R     interface{} `json:"radius,omitempty"`
	SideA interface{} `json:"side a,omitempty"`
	SideB interface{} `json:"side b,omitempty"`
	SideC interface{} `json:"side c,omitempty"`
	//ShapeArea float64
}

/*
Area:
Calculate an area of GeneralShape instance with type one of "circle", "square", "rectangle", "triangle".
Otherwise returns an error
*/
func (generalShape GeneralShape) Area() (float64, error) {
	switch generalShape.Type {
	case "circle":
		R, err := InterfaceToFloat64(generalShape.R)
		if err != nil {
			return 0, err
		}
		return math.Pi * R * R / 2, nil
	case "square":
		sideA, err := InterfaceToFloat64(generalShape.SideA)
		if err != nil {
			return 0, err
		}
		return sideA * sideA, nil
	case "rectangle":
		sideA, err := InterfaceToFloat64(generalShape.SideA)
		if err != nil {
			return 0, err
		}
		sideB, err := InterfaceToFloat64(generalShape.SideB)
		if err != nil {
			return 0, err
		}
		return sideA * sideB, nil
	case "triangle":
		sideA, err := InterfaceToFloat64(generalShape.SideA)
		if err != nil {
			return 0, err
		}
		sideB, err := InterfaceToFloat64(generalShape.SideB)
		if err != nil {
			return 0, err
		}
		sideC, err := InterfaceToFloat64(generalShape.SideC)
		if err != nil {
			return 0, err
		}
		p := (sideA + sideB + sideC) / 2
		return math.Sqrt(p * (p - sideA) * (p - sideB) * (p - sideC)), nil

	default:
		return 0, errors.New(fmt.Sprintf("unknown shape '%s'", generalShape.Type))
	}

}

/*
InterfaceToFloat64:
Converts interface that has value of types int, uint, string to float64.
params:
	i - interface
returns:
	float64
	error - if error occurred while parsing interface's value
*/
func InterfaceToFloat64(i interface{}) (float64, error) {
	reflectType := reflect.TypeOf(i)
	reflectValue := reflect.ValueOf(i)
	switch reflectType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		fmt.Println("in Int")
		return float64(reflectValue.Int()), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return float64(reflectValue.Uint()), nil
	case reflect.Float64, reflect.Float32:
		return reflectValue.Float(), nil
	case reflect.String:
		val, err := strconv.ParseFloat(reflectValue.String(), 64)
		if err != nil {
			return 0, errors.New(fmt.Sprintf("wrong value is provided. Could not convert string '%s' to float64", reflectValue.String()))
		}
		return val, nil
	default:
		return 0, errors.New("could not parse float64")
	}

}

// General shape with optional fields END

// Circle START
type Circle struct {
	R float64
}

func (circle Circle) Area() float64 {
	return math.Pi * circle.R * circle.R / 2
}

// Circle END

// Square START
type Square struct {
	A float64
}

func (square Square) Area() float64 {
	return square.A * square.A
}

// Square END

// Rectangle START
type Rectangle struct {
	A, B float64
}

func (rectangle Rectangle) Area() float64 {
	return rectangle.A * rectangle.B
}

// Rectangle END

// Triangle START
type Triangle struct {
	A, B, C float64
}

func (triangle Triangle) Area() float64 {
	p := (triangle.A + triangle.B + triangle.C) / 2
	return math.Sqrt(p * (p - triangle.A) * (p - triangle.B) * (p - triangle.C))

}

// Triangle END
